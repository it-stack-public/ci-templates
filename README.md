# ci-templates

Most of the Templates expect an initial version tag to be created.

To do this, use the following:

```
$ git tag
$ git tag 1.0.0
$ git tag
1.0.0
$ git push origin --tags
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.com:GROUP/project.git
 * [new tag]         1.0.0 -> 1.0.0
```
