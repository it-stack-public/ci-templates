#!/usr/bin/env python

import sys
import argparse

import re

REGEX_VERSION = (
    r"^(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(?P<dev>\.[a-zA-Z0-9]+|)$"
)


def main():
    parser = argparse.ArgumentParser(description="return new version")
    parser.add_argument("-version", help="existing version", required=True)
    parser.add_argument("-major", help="bump major", action="store_true")
    parser.add_argument("-minor", help="bump minor", action="store_true")
    parser.add_argument("-patch", help="bump patch", action="store_true")
    args = parser.parse_args()

    match = re.match(REGEX_VERSION, args.version)
    if match is None:
        print(f"cannot extract version from '{args.version}'", file=sys.stderr)
        return 1
    else:
        print(f"version supplied is '{args.version}'", file=sys.stderr)
    major = int(match.group("major"))
    minor = int(match.group("minor"))
    patch = int(match.group("patch"))
    dev = match.group("dev")
    if args.major is True:
        major = major + 1
        minor = 0
        patch = 0
    elif args.minor is True:
        minor = minor + 1
        patch = 0
    else:
        patch = patch + 1
    print(f"{major}.{minor}.{patch}")
    return 0


if __name__ == "__main__":
    sys.exit(main())

